package th.ac.tu.siit.lab7database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//This is a class that manages the structure of data tables
public class DBHelper extends SQLiteOpenHelper {
	
	private static final String DBNAME = "contacts.db";
	private static final int DBVERSION = 1;
	
	public DBHelper(Context ctx) {
		super(ctx, DBNAME, null, DBVERSION);
	}

	@Override
	//Called when the app is newly installed
	//use when u don't have a file, No database file in the internal storage
	public void onCreate(SQLiteDatabase db) {
		// The  primary key of the table needs to be "_id"
		//when we want to use this table w/ ListView
		String sql = "CREATE TABLE contacts (" +
				"_id integer primary key autoincrement, " +
				"ct_name text not null, " +
				"ct_phone text not null, " +
				"ct_type integer default 0, " +
				"ct_email text not null);";
		db.execSQL(sql);		
	}
	//Called when the database file exists,
	//but the DBVERSION was increased
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//Upgrade by removing the current table and recreate a new one.
		//This is NOT a practical way.
		//You should use ALTER when you need to change the structure of the table.
		String sql = "DROP TABLE IF EXISTS contacts;";
		db.execSQL(sql);
		this.onCreate(db);
	}

}


